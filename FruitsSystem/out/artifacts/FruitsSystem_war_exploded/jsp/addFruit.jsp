<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加水果</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/reset.css">
    <style>
        .form-horizontal{
            margin-top: 20px;
            font-size: 17px;
        }
        .form-horizontal .form-group{
            margin-left: 25%;
            width: 500px;
        }
    </style>
</head>
<body>
<div class="container">
    <form class="form-horizontal" method="POST" action="/Add">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">产&nbsp;品</label>
            <div class="col-sm-10">
                <input type="text" name="name" class="form-control" id="name" placeholder="请输入产品名">
            </div>
        </div>
        <div class="form-group">
            <label for="sort" class="col-sm-2 control-label">品&nbsp;种</label>
            <div class="col-sm-10">
                <input type="text" name="sort" class="form-control" id="sort" placeholder="请输入品种名">
            </div>
        </div>
        <div class="form-group">
            <label for="place" class="col-sm-2 control-label">产&nbsp;地</label>
            <div class="col-sm-10">
                <input type="text" name="place" class="form-control" id="place" placeholder="请输入产地名">
            </div>
        </div>
        <div class="form-group">
            <label for="weight" class="col-sm-2 control-label">重&nbsp;量</label>
            <div class="col-sm-10">
                <input type="text" name="weight" class="form-control" id="weight" placeholder="请输入总重量">
            </div>
        </div>
        <div class="form-group">
            <label for="price" class="col-sm-2 control-label">价&nbsp;格</label>
            <div class="col-sm-10">
                <input type="text" name="price" class="form-control" id="price" placeholder="请输入价格">
            </div>
        </div>
        <div class="form-group">
            <label for="info" class="col-sm-2 control-label" style="padding: inherit;">详细信息</label>
            <div class="col-sm-10">
                <textarea type="text" name="info" class="form-control" id="info" rows="3" placeholder="请输入详细信息"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-default" style="float: right;">Submit</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>
