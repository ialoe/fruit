<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>登录页面</title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/reset.css">
</head>

<body>
    <div class="container">
		<div class="form row">
			<form class="form-horizontal col-sm-offset-5 col-md-offset-5" id="login_form" method="POST" action="/Login">
				<h3 class="form-title">登录账户</h3>
				<div class="col-sm-5 col-md-5">
					<div class="form-group">
						<i class="fa fa-user fa-lg"></i>
						<input class="form-control required" type="text" placeholder="用户名或手机号" name="username" autofocus="autofocus" maxlength="11"/>
					</div>
					<div class="form-group">
							<i class="fa fa-lock fa-lg"></i>
							<input class="form-control required" type="password" placeholder="密码" name="password" minlength="6" maxlength="16"/>
					</div>
					<div class="form-group">
						<label class="checkbox">
							<input type="checkbox" name="remember" value="1"/>记住密码
						</label>
						<hr />
						<a href="/jsp/register.jsp" id="register_btn" class="">注册账户</a>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-success pull-right" value="登录"/>   
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>