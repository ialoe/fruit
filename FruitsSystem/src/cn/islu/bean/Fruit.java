package cn.islu.bean;

public class Fruit {
    private int fruitid;
    private String fruitname;
    private String fruitsort;
    private String fruitplace;
    private String fruitweight;
    private double fruitprice;
    private String fruitinfo;

    public Fruit() {
    }

    public Fruit(int fruitid, String fruitname, String fruitsort, String fruitplace, String fruitweight, double fruitprice, String fruitinfo) {
        this.fruitid = fruitid;
        this.fruitname = fruitname;
        this.fruitsort = fruitsort;
        this.fruitplace = fruitplace;
        this.fruitweight = fruitweight;
        this.fruitprice = fruitprice;
        this.fruitinfo = fruitinfo;
    }

    public int getFruitid() {
        return fruitid;
    }

    public void setFruitid(int fruitid) {
        this.fruitid = fruitid;
    }

    public String getFruitname() {
        return fruitname;
    }

    public void setFruitname(String fruitname) {
        this.fruitname = fruitname;
    }

    public String getFruitsort() {
        return fruitsort;
    }

    public void setFruitsort(String fruitsort) {
        this.fruitsort = fruitsort;
    }

    public String getFruitplace() {
        return fruitplace;
    }

    public void setFruitplace(String fruitplace) {
        this.fruitplace = fruitplace;
    }

    public String getFruitweight() {
        return fruitweight;
    }

    public void setFruitweight(String fruitweight) {
        this.fruitweight = fruitweight;
    }

    public double getFruitprice() {
        return fruitprice;
    }

    public void setFruitprice(double fruitprice) {
        this.fruitprice = fruitprice;
    }

    public String getFruitinfo() {
        return fruitinfo;
    }

    public void setFruitinfo(String fruitinfo) {
        this.fruitinfo = fruitinfo;
    }

    @Override
    public String toString() {
        return "Fruit{" +
                "fruitid=" + fruitid +
                ", fruitname='" + fruitname + '\'' +
                ", fruitsort='" + fruitsort + '\'' +
                ", fruitplace='" + fruitplace + '\'' +
                ", fruitweight='" + fruitweight + '\'' +
                ", fruitprice=" + fruitprice +
                ", fruitinfo='" + fruitinfo + '\'' +
                '}';
    }
}
