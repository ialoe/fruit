package cn.islu.bean;

public class User {
    private int userid;
    private String username;
    private String userphone;
    private String userpassword;

    public User() {
    }

    public User(int userid, String username, String userphone, String userpassword) {
        this.userid = userid;
        this.username = username;
        this.userphone = userphone;
        this.userpassword = userpassword;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserphone() {
        return userphone;
    }

    public void setUserphone(String userphone) {
        this.userphone = userphone;
    }

    public String getUserpassword() {
        return userpassword;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword;
    }

}