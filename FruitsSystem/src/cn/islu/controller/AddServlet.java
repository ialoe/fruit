package cn.islu.controller;

import cn.islu.service.impl.FruitServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Add")
public class AddServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Object[] params = {req.getParameter("name"), req.getParameter("sort"), req.getParameter("place"), req.getParameter("weight"), req.getParameter("price"), req.getParameter("info")};
        if (new FruitServiceImpl().addFruit(params)) {
            resp.getWriter().append("<script>\n" +
                    "alert(\"添加成功\")\n" +
                    "window.location.href = \"/Index\"" +
                    "</script>");
        } else {
            resp.getWriter().append("<script>\n" +
                    "alert(\"添加失败,请重新尝试\")\n" +
                    "window.location.href = \"/jsp/addFruit.jsp\"" +
                    "</script>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
