package cn.islu.controller;

import cn.islu.service.impl.FruitServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Delete")
public class DeleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int fruitId = Integer.parseInt(req.getParameter("fruitId"));
        if (new FruitServiceImpl().deleteFruit(fruitId)) {
            resp.sendRedirect("/Index");
        } else {
            resp.getWriter().append("<script>\n" +
                    "alert(\"删除失败,请重新尝试\")\n" +
                    "window.location.href = \"/Index\"" +
                    "</script>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
