package cn.islu.controller;

import cn.islu.bean.Fruit;
import cn.islu.service.impl.FruitServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Details")
public class DetailsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int fruitId = Integer.parseInt(req.getParameter("fruitId"));
        Fruit fruit = new FruitServiceImpl().detailsFruit(fruitId);
        if (fruit != null) {
            req.setAttribute("fruit", fruit);
            req.getRequestDispatcher("/jsp/Detailsfruit.jsp").forward(req, resp);
        } else {
            resp.getWriter().append("<script>\n" +
                    "alert(\"查看详情失败,请重新尝试\")\n" +
                    "window.location.href = \"Index\"" +
                    "</script>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
