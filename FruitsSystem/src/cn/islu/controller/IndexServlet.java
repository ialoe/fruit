package cn.islu.controller;

import cn.islu.bean.Fruit;
import cn.islu.bean.PageInfo;
import cn.islu.service.impl.FruitServiceImpl;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/Index")
public class IndexServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        Object username = null;
        if (session != null) {
            username = session.getAttribute("susername");
            req.setAttribute("username", username);
        }

        String pageNo = req.getParameter("pageNo");
        int pageSize = 5;
        PageInfo<Fruit> pageInfo = new FruitServiceImpl().findFruitsByPage(pageNo, pageSize);
        req.setAttribute("pageInfo", pageInfo);
        req.setAttribute("fruits", pageInfo.getList());
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
