package cn.islu.controller;

import cn.islu.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Register")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("password").equals(req.getParameter("rpassword"))) {
            Object[] params = {req.getParameter("username"), req.getParameter("phone"), req.getParameter("rpassword")};
            if (new UserServiceImpl().RegisterUser(params)) {
                resp.sendRedirect("/jsp/login.jsp");
            } else {
                resp.getWriter().append("<script>\n" +
                        "alert(\"注册失败,请重新尝试\")\n" +
                        "window.location.href = \"/jsp/register.jsp\"" +
                        "</script>");
            }
        } else {
            resp.getWriter().append("<script>\n" +
                    "alert(\"密码输入不一致,请重新输入\")\n" +
                    "window.location.href = \"/jsp/register.jsp\"" +
                    "</script>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
