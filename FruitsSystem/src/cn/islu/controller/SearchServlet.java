package cn.islu.controller;

import cn.islu.bean.Fruit;
import cn.islu.dao.impl.FruitDaoImpl;
import cn.islu.service.impl.FruitServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/Search")
public class SearchServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Fruit> QueryToFruit = new FruitServiceImpl().SearchFruit(req.getParameter("search"));
        if (QueryToFruit != null) {
            req.setAttribute("QueryToFruit", QueryToFruit);
            req.getRequestDispatcher("/jsp/search.jsp").forward(req, resp);
        } else {
            resp.getWriter().append("<script>\n" +
                    "alert(\"未查询到该类别水果\")\n" +
                    "window.location.href = \"/Index\"" +
                    "</script>");
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
