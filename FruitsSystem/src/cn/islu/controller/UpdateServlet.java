package cn.islu.controller;

import cn.islu.service.impl.FruitServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Update")
public class UpdateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Object[] params = {req.getParameter("name"), req.getParameter("sort"), req.getParameter("place"), req.getParameter("weight"), req.getParameter("price"), req.getParameter("info"), req.getParameter("id")};
        if (new FruitServiceImpl().updateFruit(params)) {
            resp.getWriter().append("<script>\n" +
                    "alert(\"信息修改成功\")\n" +
                    "window.location.href = \"Index\"" +
                    "</script>");
        } else {
            resp.getWriter().append("<script>\n" +
                    "alert(\"信息修改失败,请重新尝试\")\n" +
                    "window.location.href = \"Index\"" +
                    "</script>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
