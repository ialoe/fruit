package cn.islu.dao;

import cn.islu.bean.Fruit;

import java.util.List;

public interface FruitDao {
    List<Fruit> AllFruitsInfo();

    boolean AddFruit(Object[] params);

    boolean DeleteFruit(int fruitId);

    boolean UpdateFruit(Object[] parames);

    List<Fruit> getFruitByPage(int start, int pageSize);

    List<Fruit> SearchFruit(String search);

}
