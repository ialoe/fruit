package cn.islu.dao.impl;

import cn.islu.bean.User;
import cn.islu.dao.UserDao;
import cn.islu.utils.JdbcUtilsOnC3P0;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class UserDaoImpl implements UserDao {
    private QueryRunner queryRunner = new QueryRunner();
    private Connection connection = JdbcUtilsOnC3P0.getConnection();


    /**
     * 将用户名，手机号，密码传入数据库
     *
     * @param params 传入账户注册用户名，手机号，密码
     * @return 返回是否传入成功
     */
    @Override
    public boolean RegisterUserInfo(Object[] params) {
        String sql = "insert into user(username,userphone,userpassword) values(?,?,?);";
        int affectedRows = 0;
        try {
            affectedRows = queryRunner.update(connection, sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return affectedRows != 0;
    }

    @Override
    public List<User> QueryUser(Object[] params) {
        String sql = "select * from user;";
        List<User> users = null;
        try {
            users = queryRunner.query(connection, sql, new BeanListHandler<User>(User.class));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }
}
