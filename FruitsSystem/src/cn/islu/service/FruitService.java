package cn.islu.service;

import cn.islu.bean.Fruit;
import cn.islu.bean.PageInfo;

import java.util.List;

public interface FruitService<T> {
    List<T> allFruitsInfo();

    boolean addFruit(Object[] params);

    boolean deleteFruit(int fruitId);

    Fruit detailsFruit(int fruitId);

    boolean updateFruit(Object[] parames);

    int getTotalCount();

    PageInfo<T> findFruitsByPage(String pageNo, int pageSize);

    List<Fruit> SearchFruit(String search);
}
