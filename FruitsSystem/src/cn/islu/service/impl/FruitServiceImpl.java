package cn.islu.service.impl;

import cn.islu.bean.Fruit;
import cn.islu.bean.PageInfo;
import cn.islu.dao.impl.FruitDaoImpl;
import cn.islu.service.FruitService;

import java.util.List;


public class FruitServiceImpl implements FruitService<Fruit> {
    @Override
    public List<Fruit> allFruitsInfo() {
        return new FruitDaoImpl().AllFruitsInfo();
    }

    @Override
    public boolean addFruit(Object[] params) {
        return new FruitDaoImpl().AddFruit(params);
    }

    @Override
    public boolean deleteFruit(int fruitId) {
        return new FruitDaoImpl().DeleteFruit(fruitId);
    }

    @Override
    public Fruit detailsFruit(int fruitId) {
        for (Fruit fruit : new FruitDaoImpl().AllFruitsInfo()) {
            if (fruit.getFruitid() == fruitId) {
                return fruit;
            }
        }
        return null;
    }

    @Override
    public boolean updateFruit(Object[] parames) {
        return new FruitDaoImpl().UpdateFruit(parames);
    }

    @Override
    public int getTotalCount() {
        return new FruitServiceImpl().allFruitsInfo().size();
    }

    @Override
    public PageInfo<Fruit> findFruitsByPage(String pageNo, int pageSize) {
        if (pageNo == null) {
            pageNo = "1";
        }
        // 类型转换，获取int类型的当前页
        int currentPage = Integer.parseInt(pageNo);

        // 获取每一页中的第一条数据：start
        int start = (currentPage - 1) * pageSize;
        List<Fruit> fruits = new FruitDaoImpl().getFruitByPage(start, pageSize);
        return new PageInfo<>(getTotalCount(), pageSize, currentPage, fruits);
    }

    @Override
    public List<Fruit> SearchFruit(String search) {
        return new FruitDaoImpl().SearchFruit(search);
    }
}
