package cn.islu.service.impl;

import cn.islu.bean.User;
import cn.islu.dao.impl.UserDaoImpl;
import cn.islu.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {
    /**
     * 将用户名，手机号，密码传入数据库
     *
     * @param params 传入账户注册用户名，手机号，密码
     * @return 返回是否传入成功
     */
    @Override
    public boolean RegisterUser(Object[] params) {
        return new UserDaoImpl().RegisterUserInfo(params);
    }

    /**
     * 判断数据库是否存在该账户及登录时是否记住账户
     *
     * @param params 传入账户用户名，手机号，是否记住密码
     * @return 0未注册，-1账户密码不匹配，1登录并记住密码，2登录但不记住密码
     */
    @Override
    public int LoginUser(Object[] params) {
        List<User> users = new UserDaoImpl().QueryUser(params);
        for (User user : users) {
            if (user.getUsername().equals(params[0])) {
                if (user.getUserpassword().equals(params[1])) {
                    if (params[2] != null) {
                        return 2;
                    }
                    return 1;
                }
                return -1;
            }
        }
        return 0;
    }

}