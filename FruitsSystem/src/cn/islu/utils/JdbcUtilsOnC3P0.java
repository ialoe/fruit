package cn.islu.utils;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 封装JdbcUtils简化操作
 */
public class JdbcUtilsOnC3P0 {
    // 提取资源
    private static Connection connection = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;
    // 数据库连接池
    private static ComboPooledDataSource pool = new ComboPooledDataSource();

    // 获取数据库连接
    public static Connection getConnection() {
        try {
            connection = pool.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    // 重载关闭资源的方法
    public static void close(Connection connection) {
        close(resultSet, statement, connection);
    }

    public static void close(Statement statement) {
        close(resultSet, statement, connection);
    }

    public static void close(Statement statement, Connection connection) {
        close(resultSet, statement, connection);
    }

    public static void close(ResultSet resultSet, Statement statement, Connection connection) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}






