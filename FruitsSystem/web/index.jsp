<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>首页</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/reset.css">
    <style>
        header{
            height: 74px;
        }
        .pagination > li > input {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }
        .pagination>li>a{
            margin-right: 0.3px;
        }
        #statistics {
            border: 0;
        }

        #statistics:hover {
            color: #337ab7;
            background-color: #fff;
        }
        .table{
            margin-bottom: 0px;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="row">
            <header>
                <div class="col-lg-1">
                    <a class="btn btn-default" href="/jsp/addFruit.jsp" role="button" style="margin:20px 0">添加水果</a>
                </div>

                <div class="col-lg-6" style="margin: 20px 13%;">
                    <form class="input-group" method="post" action="/Search">
                        <input type="text" class="form-control" name="search" placeholder="搜索类别...">
                        <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">Go!</button>
                  </span>
                    </form>
                </div>
                <c:if test="${username==null}">
                    <a class="btn btn-default" href="/jsp/login.jsp" role="button"
                       style="margin:20px 0;float: right">登录账户</a>
                </c:if>
                <c:if test="${username!=null}">
                    <div style="margin:20px 0;float: right">
                        <span>${username}</span>
                        <a class="btn btn-default" href="/Logout" role="button">退出登录</a>
                    </div>
                </c:if>
            </header>
            <main style="min-height: 250.4px;">
                <table class="table table-bordered text-center">
                    <tr>
                        <th class="col-md-1">ID</th>
                        <th class="col-md-1">类别</th>
                        <th class="col-md-1">品种</th>
                        <th class="col-md-1">产地</th>
                        <th class="col-md-2">总重量(公斤)</th>
                        <th class="col-md-2">单价(元/公斤)</th>
                        <th class="col-md-1">删除</th>
                        <th class="col-md-1">详情</th>
                    </tr>
                    <c:forEach var="fruit" items="${fruits}">
                        <tr>
                            <td>${fruit.fruitid}</td>
                            <td>${fruit.fruitname}</td>
                            <td>${fruit.fruitsort}</td>
                            <td>${fruit.fruitplace}</td>
                            <td>${fruit.fruitweight}</td>
                            <td>${fruit.fruitprice}</td>
                            <td>
                                <a onclick="
                                        if (confirm('是否删除')) {
                                        window.location.href='Delete?fruitId=${fruit.fruitid}';
                                        }">
                                    <span class="glyphicon glyphicon-trash"></span></a>
                            </td>
                            <td><a href="/Details?fruitId=${fruit.fruitid}"><span
                                    class="glyphicon glyphicon-edit"></span></a>
                            </td>
                        </tr>
                    </c:forEach>
                    <script>
                        function deleteThis() {
                            if (confirm("是否删除")) {
                                window.location.href = "/Delete?fruitId=${fruit.fruitid}";
                            }
                        }
                    </script>
                </table>
            </main>
        <form aria-label="Page navigation" class="text-right" action="/Index" method="Post">
            <ul class="pagination">
                <c:if test="${pageInfo.pageNo == 1}">
                    <li>
                        <a href="javascript:return false;" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                </c:if>
                <c:if test="${pageInfo.pageNo != 1}">
                    <li>
                        <a href="/Index?pageNo=${pageInfo.pageNo - 1}" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                </c:if>
                <c:forEach var="pageNo" varStatus="pageStatus" begin="1" end="${pageInfo.pageCount}">
                    <c:if test="${pageNo == pageInfo.pageNo}">
                        <li><input type="submit" name="pageNo" value="${pageNo}" disabled></li>
                    </c:if>

                    <%--非当前页设置可点击--%>
                    <c:if test="${pageNo != pageInfo.pageNo}">
                        <li><input type="submit" name="pageNo" value="${pageNo}"></li>
                    </c:if>
                </c:forEach>
                <c:if test="${pageInfo.pageNo == pageInfo.pageCount}">
                    <li>
                        <a href="javascript:return false;" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </c:if>
                <c:if test="${pageInfo.pageNo != pageInfo.pageCount}">
                    <li>
                        <a href="/Index?pageNo=${pageInfo.pageNo + 1}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </c:if>
                <li><span id="statistics">共${pageInfo.totalCount}条内容，共${pageInfo.pageCount}页</span></li>
            </ul>
        </form>

    </div>

</div>
</body>

</html>