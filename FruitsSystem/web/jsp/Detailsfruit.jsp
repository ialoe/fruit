<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${fruit.fruitname}</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/reset.css">
    <style>
        .form-horizontal {
            margin-top: 20px;
            font-size: 17px;
        }

        .form-horizontal .form-group {
            margin-left: 25%;
            width: 500px;
        }
    </style>
</head>
<body>
<div class="container">
    <form class="form-horizontal" method="POST" action="/Update">
        <div class="form-group">
            <label for="id" class="col-sm-2 control-label">编&nbsp;号</label>
            <div class="col-sm-10">
                <input type="text" name="id" class="form-control" readonly id="id" value="${fruit.fruitid}">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">产&nbsp;品</label>
            <div class="col-sm-10">
                <input type="text" name="name" class="form-control" id="name" value="${fruit.fruitname}">
            </div>
        </div>
        <div class="form-group">
            <label for="sort" class="col-sm-2 control-label">品&nbsp;种</label>
            <div class="col-sm-10">
                <input type="text" name="sort" class="form-control" id="sort" value="${fruit.fruitsort}">
            </div>
        </div>
        <div class="form-group">
            <label for="place" class="col-sm-2 control-label">产&nbsp;地</label>
            <div class="col-sm-10">
                <input type="text" name="place" class="form-control" id="place" value="${fruit.fruitplace}">
            </div>
        </div>
        <div class="form-group">
            <label for="weight" class="col-sm-2 control-label">重&nbsp;量</label>
            <div class="col-sm-10">
                <input type="text" name="weight" class="form-control" id="weight" value="${fruit.fruitweight}">
            </div>
        </div>
        <div class="form-group">
            <label for="price" class="col-sm-2 control-label">价&nbsp;格</label>
            <div class="col-sm-10">
                <input type="text" name="price" class="form-control" id="price" value="${fruit.fruitprice}">
        </div>
        </div>
        <div class="form-group">
            <label for="info" class="col-sm-2 control-label" style="padding: inherit;">详细信息</label>
            <div class="col-sm-10">
                <textarea type="text" name="info" class="form-control" id="info" rows="3"></textarea>
                <script>
                    document.getElementById("info").value="${fruit.fruitinfo}";
                </script>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a class="btn btn-default" href="/Index" role="button">Back</a>
                <button type="submit" class="btn btn-default" style="float: right;">Submit</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>

