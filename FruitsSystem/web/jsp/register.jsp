<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>登录页面</title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/reset.css">
    <style>
        h3 {
            margin: 130px 0 20px 130px;
        }
    </style>
</head>

<body>
    <div class="container">
    <div class="form row">
        <form class="form-horizontal col-sm-offset-5 col-md-offset-5" id="register_form" method="POST" action="/Register">
            <h3 class="form-title">注册账户</h3>
            <div class="col-sm-5 col-md-5">
                <div class="form-group">
                    <i class="fa fa-user fa-lg"></i>
                    <input class="form-control required" type="text" placeholder="请输入用户名" name="username" minlength="6" maxlength="11"
                        autofocus="autofocus" />
                </div>
                <div class="form-group">
                    <i class="fa fa-lock fa-lg"></i>
                    <input class="form-control required" type="password" placeholder="请输入密码" id="register_password" minlength="6" maxlength="16" name="password" />
                </div>
                <div class="form-group">
                    <i class="fa fa-check fa-lg"></i>
                    <input class="form-control required" type="password" placeholder="再次输入密码" minlength="6" maxlength="16" name="rpassword" />
                </div>
                <div class="form-group">
                    <i class="fa fa-envelope fa-lg"></i>
                    <input class="form-control eamil" type="text" placeholder="请输入手机号" name="phone" minlength="11" maxlength="11" />
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success pull-right" value="注册" />
                    <input type="reset" class="btn btn-info pull-left" id="back_btn" value="清空" />
                </div>
            </div>
        </form>
    </div>
    </div>
</body>

</html>