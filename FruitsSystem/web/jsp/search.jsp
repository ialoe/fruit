<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>首页</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/reset.css">
    <style>
        header{
            height: 74px;
        }
        .table{
            margin-top: 14.4px;
        }
        .pagination > li > input {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
            <header>
                <div class="col-lg-1">
                    <a class="btn btn-default" href="/Index" role="button" style="margin:20px 0">返回首页</a>
                </div>
            </header>
        <div style="min-height: 250.4px;">
            <table class="table table-bordered text-center">
                <tr>
                    <th class="col-md-1">ID</th>
                    <th class="col-md-1">类别</th>
                    <th class="col-md-1">品种</th>
                    <th class="col-md-1">产地</th>
                    <th class="col-md-2">总重量(公斤)</th>
                    <th class="col-md-2">单价(元/公斤)</th>
                    <th class="col-md-1">删除</th>
                    <th class="col-md-1">详情</th>
                </tr>
                <c:forEach var="fruit" items="${QueryToFruit}">
                    <tr>
                        <td>${fruit.fruitid}</td>
                        <td>${fruit.fruitname}</td>
                        <td>${fruit.fruitsort}</td>
                        <td>${fruit.fruitplace}</td>
                        <td>${fruit.fruitweight}</td>
                        <td>${fruit.fruitprice}</td>
                        <td>
                            <a onclick="
                                    if (confirm('是否删除')) {
                                    window.location.href='Delete?fruitId=${fruit.fruitid}';
                                    }">
                                <span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                        <td><a href="/Details?fruitId=${fruit.fruitid}"><span
                                class="glyphicon glyphicon-edit"></span></a>
                        </td>
                    </tr>
                </c:forEach>
                <script>
                    function deleteThis() {
                        if (confirm("是否删除")) {
                            window.location.href = "/Delete?fruitId=${fruit.fruitid}";
                        }
                    }
                </script>
            </table>
        </div>
    </div>

</div>
</body>

</html>