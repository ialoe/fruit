# fruit-system

#### 介绍
在JSP页面对数据库中水果数据实现增删改查(页面待美化...)

#### 演示效果视频

https://cdn.jsdelivr.net/gh/ialoe/BlogPicture@master/fruit.wmv

#### 使用说明

1.  使用 Navicat 运行 `SQL.md` 命令进行数据库的库表操作
2.  使用IDEA(已配置tomcat环境)导入 `FruitsSystem` 文件夹
3.  运行tomcat服务器，在浏览器查看效果
