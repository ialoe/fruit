```mysql
# 创建水果项目数据库
create database fruits charset=utf8;
# 使用水果项目数据库
use fruits;
# 创建用户信息权限表
create table user(
	uid int primary key auto_increment,
	username varchar(11) not null,
	userphone varchar(11) not null,
	userpassword varchar(16) not null
)charset=utf8;
# 创建水果种类表
create table fruit(
	fruitid int primary key auto_increment,
	fruitname varchar(10) not null,
	fruitsort varchar(10) not null,
	fruitplace varchar(20) not null,
	fruitweight int not null,
	fruitprice float not null,
	fruitinfo varchar(50)
)charset=utf8;

# 添加水果
insert into fruit(fruitname,fruitsort,fruitplace,fruitweight,fruitprice,fruitinfo) values
('榴莲','猫山王榴莲','彭亨州',111,9.99,'榴莲人称水果之王，属于大补水果。'),
('樱桃','车厘子','新西兰',222,9.98,'车厘子含铁量高，营养成份为水果之首。'),
('荔枝','红毛丹','泰国',333,9.97,'红毛丹果壳洗净加水煎煮当茶饮，可改善口团炎与腹泻。'),
('软枣','牛奶枣','台湾',444,9.96,'软枣不是枣，它的学名为君迁子，又名黑枣，为柿树科、柿属植物，与枣不同科，与柿树才是同科植物。'),
('蓝莓','龙果','北美洲',555,9.95,'青花素含量较多，有很好的抗氧化作用。虽然价格贵点，不过却美味又营养，对视力也能起到很好的保护。');
```

